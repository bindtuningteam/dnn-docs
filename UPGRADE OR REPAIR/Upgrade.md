- [Before upgrading](#beforeupgrading)
- [Upgrading your theme](#upgradingyourtheme)

**Warning:** ALWAYS make a backup of all of your theme assets, including CSS files, master pages, page layouts,... Upgrading the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading your theme clean your browser cache.

<a name="beforeupgrading"></a>
##Before upgrading 
Before upgrading your theme you will need to request the new version at BindTuning.com. You can find all the instructions you need to upgrade your theme's version [here](http://support.bind.pt/hc/en-us/articles/204447149-How-do-I-request-an-update-for-my-theme-).

![upgrade_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/upgrade_1.png)


Download the new upgraded version from your account and save it with the same name as the theme you have installed. In our case is *MyTheme*.UMB7.zip. 

<a name="upgradingyourtheme"></a>
##Upgrading your theme 
Now that you have downloaded the upgraded version of your theme the only thing left to do is to uprage the theme you've installed on your website.


Select your Umbraco version:

- [Version 7 (only MVC)](#version7)
- [Version 4-6](#version4-6) 


<a name="version7"></a>
###Version 7 (only MVC)

Ready for this? Here we go: 

1. Open your Umbraco admin area; 

2. On the left menu, **click on *Developer***; 

3. Open the *Packages* folder and **click on *Install local package***;

4. **Select your theme package**. Make sure the theme package has the same name as the one you have installed; 

5. **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;

	![upgrade_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/upgrade_2.png)

6. A message saying "Stylesheet Conflicts in the package!" will appear. Ignore it;

7. **Check *Accept license*** and **click on *Install Package***;

	![upgrade_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/upgrade_3.png)

8. And that's it. You should get a message saying: *view installed package* or *view package website*. 

------

<a name="version4-6"></a>
###Version 4-6

Ready for this? Here we go: 

1. Open your Umbraco admin area; 

2. **click on *Developer***; 

3. Open the *Packages* folder and **click on *Install local package***;

4. **Select your theme package**. Make sure the theme package has the same name as the one you have installed; 

5. **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;

	![install_10](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/install_10.png)

6. A message saying "Stylesheet Conflicts in the package!" will appear. Ignore it;

7. **Check *Accept license*** and **click on *Install Package***;

	![upgrade_4](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/upgrade_4.png)

8. And that's it. You should get a message saying: *view installed package* or *view package website*. 

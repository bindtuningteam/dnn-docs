**Warning:** ALWAYS make a backup of all of your theme assets, including CSS files, master pages, page layouts,... Repairing the theme will remove all custom changes you have applied previously.

**Important:** Before upgrading or repairing your theme, clean your browser cache.


Repairing your theme is as easy as starting your browser. The only thing you need to do is install the theme package again and that's it.

First select the Umbraco version you are working with:

- [Version 7 (only MVC)](#version7)
- [Version 4-6](#version4-6)


<a name="version7"></a>
###Version 7 (only MVC)
Follow these steps:

1. **Open your Umbraco admin area**;
2. On the left menu, **click on *Developer***;
3. Open the *Packages* folder and **click on *Install local package***;
4. **Select your theme package**. Make sure the theme package has the same name as the one you have installed;
5.  **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;

	![repair_1](https://bitbucket.org/bindtuningteam/dnn-docs/wiki/images/repair_1.png)

6.  A message saying "Stylesheet Conflicts in the package!" will appear. Ignore it;
7. **Check *Accept license*** and **click on *Install Package***;

	![repair_2](https://bitbucket.org/bindtuningteam/dnn-docs/wiki/images/repair_2.png)

8. And that's it. You should get a message saying: *view installed package* or *view package website*.


<a name="version4-6"></a>
###Version 4-6
Follow these steps:

1. **Open your DNN admin area**;
2. **Click on *Developer***;
3. Open the *Packages* folder and **click on *Install local package***;
4. **Select your theme package**. Make sure the theme package has the same name as the one you have installed;
5.  **Check “I understand the security risks associated with installing a local package”** and **click on *Load Package***;

	![repair_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/repair_1.png)

6.  A message saying "Stylesheet Conflicts in the package!" will appear. Ignore it;
7. **Check *Accept license*** and **click on *Install Package***;

	![repair_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/repair_2.png)

8. And that's it. You should get a message saying: *view installed package* or *view package website*.

Select the DNN version you are working with:

- [Version 9](#version9)

- [Version 7 & 8](#version7-8)


<!--<a name="version9"></a>
##Version 9
### Change the Template-->

##Version 7-8
### 1# Change the Template

Step 1 of uninstalling any BindTuning theme from DNN is to change the Master Template in case you are using any of the theme's templates.

1. Login to your DNN admin area;

2. Click on **Site Settings** under **Admin** on the top bar;

  <!--![uninstall_1](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN 8 Uninstall - 1.jpg)-->
 
3. Scroll down to the **Appearance** section and open it;

4. Change all instances of the theme you want to remove to a different one;

   <!--![uninstall_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN 8 Uninstall - 2.jpg)-->
   
5. Click **Update** and you're done;


### 2# Delete the theme

1. Login to your DNN admin area;

2. Click on **Extensions** under **Host** on the top bar;

   <!--![uninstall_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN 8 Uninstall - 3.jpg)-->

3. Scroll down to the **Themes** section and find the theme you want to uninstall;

4. Click the **Uninstall this Extension** icon to the right;
 
  <!--![uninstall_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN 8 Uninstall - 4.jpg)-->

5. Check **Delete Files** and click **Uninstall Package**;

6. You should get a message saying "Package Uninstall Successful".


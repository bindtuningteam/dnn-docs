- [Version 9](#version9)
- [Version 7 & 8](#version7-8)

BindTuning themes include a variety of responsive master templates that you can use and select for different parts of your site.   


<a name="version9"></a>
##Version 9
To apply one of BindTuning themes master templates follow these steps:

1. In your admin area, go to the **Manage** section on the persona bar and click **Themes**;

  	<!--![setthemasterpage_1](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - Masterpage 1.jpg)--> 

2. Hover above your theme and click on the **Apply Theme** icon in the middle and select **Confirm**;

  	<!--![setthemasterpage_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - Masterpage 2.jpg)-->  

3. Now you can select any of the masterpages available to you in the **Site Theme** section.

  <!--![setthemasterpage_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - Masterpage 3.jpg)-->  

-------

<a name="version7-8"></a>
##Version 7 & 8
To apply one of BindTuning themes master templates follow these steps:

1. Mouse over to **Admin** and click **Site Settings**;

  <!--![setthemasterpage_4](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN8 - Masterpage 1.jpg)-->

2. At the bottom of the page open up the **Appearance** tab;

3. Set the **Site Theme** to your desired master page and click **Update**; 

  <!--![setthemasterpage_5](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN8 - Masterpage 2.jpg)-->

<!-------------->

<!--**Next** - [Upload the demo content](http://bindtuning-umbraco-themes.readthedocs.io/en/latest/SETUP/Upload%20the%20demo%20content/)-->





- [Download the theme](#downloadthetheme)
- [Inside the .zip file](#insidethe.zipfile)


<a name="downloadthetheme"></a>
## Download the theme ###
Ready to get started? First you need to download your theme's zip file from bindtuning.com. **Access your theme details page** and **click on the *Download* button** to download your theme.

<!--![gettingstarted_1](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN Download.jpg)-->

<a name="insidethe.zipfile"></a>
## Inside the .zip file ###
Let's take a look at what the zip file contains. Inside your theme package you will find two folders, one for Containers and the other one with Skin Files.

- ***yourthemename*SkinFiles**

  Inside the *yourthemename*SkinFiles folder you will find JavaScript files, PNG files, GIF files, CSS files, etc.

  <!--![gettingstarted_4](https://bitbucket.org/bindtuningteam/dnn-docs/wiki/images/gettingstarted_4.png)-->


Now let's get your website rocking! First thing on the list is go through all the [Requirements](./INSTALL/Requirements.md) before installing the theme.

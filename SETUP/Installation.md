- [Version 9](#version9)
- [Version 7 & 8](#version7-8)


<a name="version9"></a>
##Version 9


1. Login to the  **DNN admin area**;

2. Mouse over the **Settings** option on the persona bar to the left and click **Extensions**;

  <!--![install_1](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - 1.jpg)--> 

3. On the top right corner of the panel click **Install Extension**;

  <!--![install_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - 2.jpg)--> 

4. Drag and drop your theme zip file or click the upload section and select it;

  <!--![install_3](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN9 - 3.jpg)--> 

5. After the upload is complete, click **Next** and go through the steps until the package is installed and click **Done**;

6. Check **“Accept license”** and click **Install Package** to install the theme;

7. And that's it. Your theme is installed and you should be able to see it in the **Installed Extensions** list! 

-----

<a name="version7-8"></a>
##Version 7 & 8


1. Login to the  **DNN admin area**;

2. Mouse over the **Host** option on the top bar to the left and click **Extensions**;

  <!--![install_1](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN8 - 1.jpg)--> 

3. On the top right corner of the page click **Install Extension**;

  <!--![install_2](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN8 - 2.jpg)--> 

4. Click the upload section and select your zip file;

<!--![install_3](C:\Bindtuning\ReadTheDocs\Themes\dnn-docs\IMAGES\DNN8 - 3.jpg)--> 

5. After the upload is complete, click **Next** and go through the steps until the package is installed and click **Done**;

6. Check **“Accept license”** and click on **"Install Package"** to install the theme;

7. And that's it. Your theme is installed and you should be able to see it in the **Installed Extensions** list! 

-----
**Next up** - [Set the Master Template](http://bindtuning-dnn-7-8-9-themes.readthedocs.io/en/latest/SETUP/Set%20the%20Master%20Template/)

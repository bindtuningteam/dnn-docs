- [About BindTuning containers](#aboutbindtuningcontainers)
- [Before using a container](#beforeusingacontainer)
- [Using a container](#usingacontainer)

<a name="aboutbindtuningcontainers"></a>
##About BindTuning containers
BindTuning themes include several containers that you can use to visually wrap your content. Inside your theme package you will find all the different containers your theme includes, each one with a different styling.

![usingcontainers_1](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_1.png)


<a name="beforeusingacontainer"></a>
##Before using a container
Since all the containers files are inside your theme package you need to first unzip your theme package.

Make sure the file has not been blocked and uzinp it to a location of your choosing. 

Ready to try them on? 

<a name="usingacontainer"></a>
##Using a Container
Which Umbraco version are you working with?

- [Version 7 (only MVC)](#version7)
- [Version 4-6](#version4-6) 


------

<a name="version7"></a>
###Version 7 (only MVC)

1. **Open the file** of the container you want to use with your favorite text editor (Notepad ++ will do the trick);

2. **Copy the structure**;

	![usingcontainers_2](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_2.png)
	
3. Now go to your admin area (http://yourURL/umbraco) and **click on the *Settings* section**;

4. **Open the *Templates* folder**;

5. **Open the page template** where you want to add the container;

6. On the Template tab **enter the code**, **also indicating the zones** where you want the content to appear; 

	![usingcontainers_3](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_3.png)
	
7. **Click *Save***. All done!

	![usingcontainers_4](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_4.png)

**Note:** If you don't need any styling around your content, you can use the container C00_Clean, which is the container default. 

------

<a name="version4-6"></a>
###Version 4-6
1. **Open the file** of the container you want to use with your favorite text editor (Notepad ++ will do the trick);

2. **Copy the structure**;

	![usingcontainers_5](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_5.png)
	
3. Now go to your admin area (http://yourURL/umbraco) and **click on the *Settings* section**;

4. **Open the *Templates* folder**;

5. **Open the page template** where you want to add the container;

6. **Enter the code**, **also indicating the zones** where you want the content to appear; 

	![usingcontainers_6](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_6.png)

7. **Click *Save***. All done!

	![usingcontainers_7](https://bitbucket.org/bindtuningteam/umbraco-docs/wiki/images/usingcontainers_7.png)

**Note:** If you don't need any styling around your content, you can use the container C00_Clean, which is the container default. 

------

What an awesome looking website! Now you can start adding your content and share your vision with the World. Good luck!